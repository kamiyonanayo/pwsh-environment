Write-Host "------------------------------------------------------------------------------------------------------------------------------------"

Import-Module "$PSScriptRoot/pwsh-environment/pwsh-environment.psm1" -Force -Verbose

# Push-Environment "aaa"
# #Push-Environment "aaa"

# #Push-Environment "bb"
# #Push-Environment "bb"
# #Push-Environment "bb"

# #$a = Get-Environment -StackName "aaa"

# #$a

# #Get-Environment -StackName "aaa"

# #$a = Get-Environment -StackName "aaa"

# $aaa = Get-Environment -StackName "aaa"


# $bbb = Get-Environment -StackName "aaa"

# $aaa_data = $aaa[0].Data.Peek()


# #$aaa_data[0].Value = $aaa_data[0].Value + "1111111"

# $aaa_data["0"].Value = $aaa_data["0"].Value + "1111111"


# $aaa[0].Data

# "@@@@@@@@@@"

# $bbb[0].Data

# "@@@@@@@@@@"

# (Get-Environment -StackName "aaa")[0].Data

# "@@@@@@@@@@"

# Set-Environment GOPATH "$HOME/gocode"
# Set-Environment GOROOT "$HOME/go"


Add-EnvironmentPath -Path "$HOME/gocode2" -EnvironmentVariableName "GOPATH"
Remove-EnvironmentPath -Path "$HOME/gocode2" -EnvironmentVariableName "GOPATH"
# Set-EnvironmentVariable GOPATH -Value ""
