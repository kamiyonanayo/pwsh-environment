$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Set-EnvironmentVariable" {

        BeforeEach {
            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
            $BasicEnvName = "${EnvTestPrefix}Test"
        }
        AfterEach {
            Remove-Item "Env:\${EnvTestPrefix}*"
        }

        It "set environment variable name" {

            Get-Content  -LiteralPath "Env:\${BasicEnvName}" -ErrorAction SilentlyContinue | Should -BeExactly $null

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable!!"

            Get-Content  -LiteralPath "Env:\${BasicEnvName}" -ErrorAction SilentlyContinue  | Should -BeExactly "variable!!"

        }

        It "set environment variable literal name" {

            Get-Content  -LiteralPath "Env:\${BasicEnvName}_*1" -ErrorAction SilentlyContinue | Should -BeExactly $null

            Set-EnvironmentVariable -Name "${BasicEnvName}_*1" -Value "variable!!"

            Get-Content  -LiteralPath "Env:\${BasicEnvName}_*1" -ErrorAction SilentlyContinue  | Should -BeExactly "variable!!"

        }

        It "set environment variable name from pipeline" {

            Get-Content  -LiteralPath "Env:\${BasicEnvName}_1" -ErrorAction SilentlyContinue | Should -BeExactly $null
            Get-Content  -LiteralPath "Env:\${BasicEnvName}_2" -ErrorAction SilentlyContinue | Should -BeExactly $null

            ([PSCustomObject]@{Name="${BasicEnvName}_1";Value="variable1!!"},[PSCustomObject]@{Name="${BasicEnvName}_2";Value="variable2!!"}) | Set-EnvironmentVariable

            Get-Content  -LiteralPath "Env:\${BasicEnvName}_1" -ErrorAction SilentlyContinue  | Should -BeExactly "variable1!!"
            Get-Content  -LiteralPath "Env:\${BasicEnvName}_2" -ErrorAction SilentlyContinue  | Should -BeExactly "variable2!!"

        }

    }
}
