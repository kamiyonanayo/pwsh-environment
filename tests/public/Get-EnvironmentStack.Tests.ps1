$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Get-EnvironmentStack" {

        BeforeEach {
            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
        }
        AfterEach {
            Clear-InternalEnvironmentStack
            Remove-Item "Env:\${EnvTestPrefix}*"
        }

        It "get environment stack" {

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A1" -Value "A1"

            Push-EnvironmentStack -Message "Message1"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_A1"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A2" -Value "A2"

            $stack = Get-EnvironmentStack

            Push-EnvironmentStack -StackName "" -Message "Message2"

            $stack.Count | Should -BeExactly 1

            $stack.Peek().Message  | Should -BeExactly "Message1"
            $stack.Peek()["${EnvTestPrefix}_A1"].Value | Should -BeExactly "A1"

            $stack2 = Get-EnvironmentStack

            $stack2.Count | Should -BeExactly 2

            $stack2.Peek().Message  | Should -BeExactly "Message2"
            $stack2.Peek()["${EnvTestPrefix}_A2"].Value | Should -BeExactly "A2"


        }

        It "get environment named stack" {

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A1" -Value "A1"

            Push-EnvironmentStack -StackName "ABC"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_A1"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A2" -Value "A2"

            $stack = Get-EnvironmentStack -StackName "ABC"

            Push-EnvironmentStack -StackName "ABC"

            $stack.Count | Should -BeExactly 1

            $stack.Peek()["${EnvTestPrefix}_A1"].Value | Should -BeExactly "A1"

            $stack2 = Get-EnvironmentStack -StackName "ABC"

            $stack2.Count | Should -BeExactly 2

            $stack2.Peek()["${EnvTestPrefix}_A2"].Value | Should -BeExactly "A2"

        }

        It "get environment named stack from pipeline" {

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A1" -Value "A1"

            Push-EnvironmentStack -StackName "ABC"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_A1"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A2" -Value "A2"

            $stack = [PSCustomObject]@{StackName = ("ABC") } | Get-EnvironmentStack

            Push-EnvironmentStack -StackName "ABC"

            $stack.Count | Should -BeExactly 1

            $stack.Peek()["${EnvTestPrefix}_A1"].Value | Should -BeExactly "A1"

            $stack2 = [PSCustomObject]@{StackName = ("ABC") } | Get-EnvironmentStack

            $stack2.Count | Should -BeExactly 2

            $stack2.Peek()["${EnvTestPrefix}_A2"].Value | Should -BeExactly "A2"

        }


        It "get environment stack top" {

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A1" -Value "A1"

            Push-EnvironmentStack -StackName "ABC"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_A1"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A2" -Value "A2"

            $stackTop = Get-EnvironmentStack -StackName "ABC" -Top

            Push-EnvironmentStack

            $stackTop["${EnvTestPrefix}_A1"].Value | Should -BeExactly "A1"

            $stack2Top = Get-EnvironmentStack -Top

            $stack2Top["${EnvTestPrefix}_A2"].Value | Should -BeExactly "A2"

        }

    }
}
