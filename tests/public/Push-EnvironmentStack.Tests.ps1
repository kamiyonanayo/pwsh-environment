$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Push-EnvironmentStack" {

        BeforeEach {
            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
            $BasicEnvName = "${EnvTestPrefix}Test"
        }
        AfterEach {
            Clear-InternalEnvironmentStack
            Remove-Item "Env:\${EnvTestPrefix}*"
        }

        It "push environment stack " {

            $script:EnvStack.Count | Should -BeExactly 0

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable!!"

            Push-EnvironmentStack

            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack[""].Count | Should -BeExactly 1

            $envs =$script:EnvStack[""].Peek();

            $envs[$BasicEnvName].Name | Should -BeExactly $BasicEnvName
            $envs[$BasicEnvName].Value | Should -BeExactly "variable!!"

        }

        It "push environment stack default name" {

            $script:EnvStack.Count | Should -BeExactly 0

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable1"

            Push-EnvironmentStack
            $script:DefaultEnvStackName = "ABC"

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable2"

            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack[""].Count | Should -BeExactly 1

            Push-EnvironmentStack

            $script:EnvStack.Count | Should -BeExactly 2
            $script:EnvStack[""].Count | Should -BeExactly 1
            $script:EnvStack.Count | Should -BeExactly 2
            $script:EnvStack["ABC"].Count | Should -BeExactly 1

            $envs =$script:EnvStack[""].Peek();

            $envs["${BasicEnvName}"].Name | Should -BeExactly "${BasicEnvName}"
            $envs["${BasicEnvName}"].Value | Should -BeExactly "variable1"

            $envs =$script:EnvStack["ABC"].Peek();

            $envs["${BasicEnvName}"].Name | Should -BeExactly "${BasicEnvName}"
            $envs["${BasicEnvName}"].Value | Should -BeExactly "variable2"


        }

        It "push environment named stack " {

            $script:EnvStack.Count | Should -BeExactly 0

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable1"

            Push-EnvironmentStack -StackName ""

            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack[""].Count | Should -BeExactly 1

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable2"

            Push-EnvironmentStack -StackName "DEF"

            $script:EnvStack.Count | Should -BeExactly 2
            $script:EnvStack[""].Count | Should -BeExactly 1
            $script:EnvStack.Count | Should -BeExactly 2
            $script:EnvStack["DEF"].Count | Should -BeExactly 1

            $envs =$script:EnvStack[""].Peek();

            $envs["${BasicEnvName}"].Name | Should -BeExactly "${BasicEnvName}"
            $envs["${BasicEnvName}"].Value | Should -BeExactly "variable1"

            $envs =$script:EnvStack["DEF"].Peek();

            $envs["${BasicEnvName}"].Name | Should -BeExactly "${BasicEnvName}"
            $envs["${BasicEnvName}"].Value | Should -BeExactly "variable2"

        }

        It "push environment stack with message" {

            $script:EnvStack.Count | Should -BeExactly 0

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable!!"

            Push-EnvironmentStack -Message "Message!!"

            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack[""].Count | Should -BeExactly 1

            $envs =$script:EnvStack[""].Peek();

            $envs.Message | Should -BeExactly "Message!!"

            $envs[$BasicEnvName].Name | Should -BeExactly $BasicEnvName
            $envs[$BasicEnvName].Value | Should -BeExactly "variable!!"

        }

        It "push environment stack from pipeline" {

            $script:EnvStack.Count | Should -BeExactly 0

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable1"

            [PSCustomObject]@{StackName="";Message="Message default!!"} | Push-EnvironmentStack

            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack[""].Count | Should -BeExactly 1

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable2"

            [PSCustomObject]@{StackName="DEF";Message="Message DEF!!"} | Push-EnvironmentStack

            $script:EnvStack.Count | Should -BeExactly 2
            $script:EnvStack[""].Count | Should -BeExactly 1
            $script:EnvStack.Count | Should -BeExactly 2
            $script:EnvStack["DEF"].Count | Should -BeExactly 1

            $envs =$script:EnvStack[""].Peek();

            $envs.Message | Should -BeExactly "Message default!!"
            $envs["${BasicEnvName}"].Name | Should -BeExactly "${BasicEnvName}"
            $envs["${BasicEnvName}"].Value | Should -BeExactly "variable1"

            $envs =$script:EnvStack["DEF"].Peek();

            $envs.Message | Should -BeExactly "Message DEF!!"
            $envs["${BasicEnvName}"].Name | Should -BeExactly "${BasicEnvName}"
            $envs["${BasicEnvName}"].Value | Should -BeExactly "variable2"

        }
    }
}
