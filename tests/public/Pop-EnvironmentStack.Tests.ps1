$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Pop-EnvironmentStack" {

        BeforeEach {
            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
            $BasicEnvName = "${EnvTestPrefix}"
        }
        AfterEach {
            Clear-InternalEnvironmentStack
            Remove-Item "Env:\${EnvTestPrefix}*"
        }

        It "pop environment stack case sensitive" {

            if(-not $script:IsCaseSensitive){
                return
            }

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 0

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_init" -Value "init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_INIT" -Value "INIT"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_update" -Value "init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_UPDATE" -Value "INIT"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_insert" -Value "init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_INSERT" -Value "INIT"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_change" -Value "init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_CHANGE" -Value "init"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 8

            $script:EnvStack.Count | Should -BeExactly 0
            Push-EnvironmentStack
            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack[""].Count | Should -BeExactly 1

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_update" -Value "update"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_UPDATE" -Value "UPDATE"

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_change" -Value "Init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_CHANGE" -Value "Init"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_insert"
            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_INSERT"

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_delete" -Value "init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_DELETE" -Value "INIT"


            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_init")      | Should -BeExactly "init"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_INIT")      | Should -BeExactly "INIT"

            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_update")    | Should -BeExactly "update"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_UPDATE")    | Should -BeExactly "UPDATE"

            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_delete")    | Should -BeExactly "init"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_DELETE")    | Should -BeExactly "INIT"

            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_change")    | Should -BeExactly "Init"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_CHANGE")    | Should -BeExactly "Init"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 8

            Pop-EnvironmentStack

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 8

            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_init")      | Should -BeExactly "init"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_INIT")      | Should -BeExactly "INIT"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_update")    | Should -BeExactly "init"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_UPDATE")    | Should -BeExactly "INIT"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_insert")    | Should -BeExactly "init"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_INSERT")    | Should -BeExactly "INIT"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_change")    | Should -BeExactly "init"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_CHANGE")    | Should -BeExactly "init"

        }

        It "pop environment stack case insensitive" {

            if($script:IsCaseSensitive){
                return
            }

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 0

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_init" -Value "init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_INIT" -Value "INIT"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_update" -Value "init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_UPDATE" -Value "INIT"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_insert" -Value "init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_INSERT" -Value "INIT"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_change" -Value "init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_CHANGE" -Value "init"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 4

            $script:EnvStack.Count | Should -BeExactly 0
            Push-EnvironmentStack
            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack[""].Count | Should -BeExactly 1

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_update" -Value "update"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_UPDATE" -Value "UPDATE"

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_change" -Value "Init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_CHANGE" -Value "Init"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_insert"
            #Remove-Item -LiteralPath "Env:${EnvTestPrefix}_INSERT"

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_delete" -Value "init"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_DELETE" -Value "INIT"


            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_init")      | Should -BeExactly "INIT"

            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_update")    | Should -BeExactly "UPDATE"

            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_delete")    | Should -BeExactly "INIT"

            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_change")    | Should -BeExactly "Init"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 4

            Pop-EnvironmentStack

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 4

            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_init")      | Should -BeExactly "INIT"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_update")    | Should -BeExactly "INIT"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_insert")    | Should -BeExactly "INIT"
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_change")    | Should -BeExactly "init"

        }

        It "pop environment named stack " {

            (Get-ChildItem "Env:\${EnvTestPrefix}*") | Should -BeNullOrEmpty

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A1" -Value "A1"

            Push-EnvironmentStack -StackName "ABC"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_A1"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A2" -Value "A2"

            Push-EnvironmentStack -StackName "DEF"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A2")      | Should -BeExactly "A2"

            Pop-EnvironmentStack -StackName "ABC"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A1")      | Should -BeExactly "A1"

            Pop-EnvironmentStack -StackName "DEF"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A2")      | Should -BeExactly "A2"

        }

        It "pop environment stack from pipeline" {

            (Get-ChildItem "Env:\${EnvTestPrefix}*") | Should -BeNullOrEmpty

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A1" -Value "A1"

            Push-EnvironmentStack -StackName "ABC"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_A1"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A2" -Value "A2"

            Push-EnvironmentStack -StackName "DEF"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A2")      | Should -BeExactly "A2"

            [PSCustomObject]@{StackName="ABC"} | Pop-EnvironmentStack

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A1")      | Should -BeExactly "A1"

            [PSCustomObject]@{StackName="DEF"} | Pop-EnvironmentStack

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A2")      | Should -BeExactly "A2"

        }

        It "pop environment peek stack " {

            (Get-ChildItem "Env:\${EnvTestPrefix}*") | Should -BeNullOrEmpty

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A1" -Value "A1"

            Push-EnvironmentStack -StackName "ABC"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_A1"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A2" -Value "A2"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A2")      | Should -BeExactly "A2"

            Pop-EnvironmentStack -StackName "ABC" -Peek

            Get-EnvironmentStack -StackName "ABC" | Should -Not -BeNullOrEmpty

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A1")      | Should -BeExactly "A1"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_A1"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A2" -Value "A2"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A2")      | Should -BeExactly "A2"

            Pop-EnvironmentStack -StackName "ABC"

            Get-EnvironmentStack -StackName "ABC" | Should -BeNullOrEmpty

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A1")      | Should -BeExactly "A1"

        }

        It "pop environment drop stack " {

            (Get-ChildItem "Env:\${EnvTestPrefix}*") | Should -BeNullOrEmpty

            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A1" -Value "A1"

            Push-EnvironmentStack -StackName "ABC"

            Remove-Item -LiteralPath "Env:${EnvTestPrefix}_A1"
            Set-Content -LiteralPath "Env:${EnvTestPrefix}_A2" -Value "A2"

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A2")      | Should -BeExactly "A2"

            Pop-EnvironmentStack -StackName "ABC" -Drop

            Get-EnvironmentStack -StackName "ABC" | Should -BeNullOrEmpty

            @(Get-ChildItem "Env:\${EnvTestPrefix}*").Count | Should -BeExactly 1
            (Get-Content -LiteralPath "Env:${EnvTestPrefix}_A2")      | Should -BeExactly "A2"

        }

        Context "ignored test" {
            BeforeEach {
                $ignored_name_list = @(
                    "COMP_WORDBREAKS" 
                    "PS1"
                    "OLDPWD"
                    "PWD"
                    "SHELL"
                    "SHELLOPTS"
                    "SHLVL"
                    "_"
                    "__fish_test"
                    "BASH_FUNC_TEST"
                )
                $backup_envs = [ordered]@{}
                foreach ($name in $ignored_name_list) {
                    $backup_envs[$name] = if(Test-Path -LiteralPath "Env:\${name}"){
                        Get-Content -LiteralPath "Env:\${name}"
                    }else{
                        $null
                    }
                }
            }
            AfterEach {
                foreach ($name in $backup_envs.Keys) {
                    if($null -eq $backup_envs[$name]){
                        if(Test-Path -LiteralPath "Env:\${name}"){
                            Remove-Item -LiteralPath "Env:\${name}"
                        }
                    }else{
                        Set-Content -LiteralPath "Env:\${name}" -Value ($backup_envs[$name])
                    }
                }
            }

            It "pop environment ignored name " {


                Set-Content -LiteralPath "Env:${EnvTestPrefix}_Test" -Value "AA"
                Set-Content -LiteralPath "Env:\COMP_WORDBREAKS"      -Value "01"
                Set-Content -LiteralPath "Env:\PS1"                  -Value "02"
                Set-Content -LiteralPath "Env:\OLDPWD"               -Value "03"
                Set-Content -LiteralPath "Env:\PWD"                  -Value "04"
                Set-Content -LiteralPath "Env:\SHELL"                -Value "05"
                Set-Content -LiteralPath "Env:\SHELLOPTS"            -Value "06"
                Set-Content -LiteralPath "Env:\SHLVL"                -Value "07"
                Set-Content -LiteralPath "Env:\_"                    -Value "08"
                Set-Content -LiteralPath "Env:\__fish_test"          -Value "09"
                Set-Content -LiteralPath "Env:\BASH_FUNC_TEST"       -Value "0A"

                Push-EnvironmentStack

                Set-Content -LiteralPath "Env:${EnvTestPrefix}_Test" -Value "BB"
                Set-Content -LiteralPath "Env:\COMP_WORDBREAKS"      -Value "11"
                Set-Content -LiteralPath "Env:\PS1"                  -Value "12"
                Set-Content -LiteralPath "Env:\OLDPWD"               -Value "13"
                Set-Content -LiteralPath "Env:\PWD"                  -Value "14"
                Set-Content -LiteralPath "Env:\SHELL"                -Value "15"
                Set-Content -LiteralPath "Env:\SHELLOPTS"            -Value "16"
                Set-Content -LiteralPath "Env:\SHLVL"                -Value "17"
                Set-Content -LiteralPath "Env:\_"                    -Value "18"
                Remove-Item -LiteralPath "Env:\__fish_test"
                Remove-Item -LiteralPath "Env:\BASH_FUNC_TEST"

                (Get-Content -LiteralPath "Env:${EnvTestPrefix}_Test") | Should -BeExactly "BB"
                (Get-Content -LiteralPath "Env:\COMP_WORDBREAKS"     ) | Should -BeExactly "11"
                (Get-Content -LiteralPath "Env:\PS1"                 ) | Should -BeExactly "12"
                (Get-Content -LiteralPath "Env:\OLDPWD"              ) | Should -BeExactly "13"
                (Get-Content -LiteralPath "Env:\PWD"                 ) | Should -BeExactly "14"
                (Get-Content -LiteralPath "Env:\SHELL"               ) | Should -BeExactly "15"
                (Get-Content -LiteralPath "Env:\SHELLOPTS"           ) | Should -BeExactly "16"
                (Get-Content -LiteralPath "Env:\SHLVL"               ) | Should -BeExactly "17"
                (Get-Content -LiteralPath "Env:\_"                   ) | Should -BeExactly "18"
                (Test-Path   -LiteralPath "Env:\__fish_test"         ) | Should -BeFalse
                (Test-Path   -LiteralPath "Env:\BASH_FUNC_TEST"      ) | Should -BeFalse

                $debug =  Pop-EnvironmentStack -Debug -Confirm:$false 5>&1

                $debug | ForEach-Object { $_.Message} | Sort-Object | Should -BeExactly @(
                    "Apply Ignored Name _"
                    "Apply Ignored Name __fish_test"
                    "Apply Ignored Name BASH_FUNC_TEST"
                    "Apply Ignored Name COMP_WORDBREAKS"
                    "Apply Ignored Name OLDPWD"
                    "Apply Ignored Name PS1"
                    "Apply Ignored Name PWD"
                    "Apply Ignored Name SHELL"
                    "Apply Ignored Name SHELLOPTS"
                    "Apply Ignored Name SHLVL"
                    "Replace Ignored Name _"
                    "Replace Ignored Name COMP_WORDBREAKS"
                    "Replace Ignored Name OLDPWD"
                    "Replace Ignored Name PS1"
                    "Replace Ignored Name PWD"
                    "Replace Ignored Name SHELL"
                    "Replace Ignored Name SHELLOPTS"
                    "Replace Ignored Name SHLVL"
                )

                (Get-Content -LiteralPath "Env:${EnvTestPrefix}_Test") | Should -BeExactly "AA"
                (Get-Content -LiteralPath "Env:\COMP_WORDBREAKS"     ) | Should -BeExactly "11"
                (Get-Content -LiteralPath "Env:\PS1"                 ) | Should -BeExactly "12"
                (Get-Content -LiteralPath "Env:\OLDPWD"              ) | Should -BeExactly "13"
                (Get-Content -LiteralPath "Env:\PWD"                 ) | Should -BeExactly "14"
                (Get-Content -LiteralPath "Env:\SHELL"               ) | Should -BeExactly "15"
                (Get-Content -LiteralPath "Env:\SHELLOPTS"           ) | Should -BeExactly "16"
                (Get-Content -LiteralPath "Env:\SHLVL"               ) | Should -BeExactly "17"
                (Get-Content -LiteralPath "Env:\_"                   ) | Should -BeExactly "18"
                (Test-Path   -LiteralPath "Env:\__fish_test"         ) | Should -BeFalse
                (Test-Path   -LiteralPath "Env:\BASH_FUNC_TEST"      ) | Should -BeFalse

            }
        }

    }
}
