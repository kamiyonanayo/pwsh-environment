$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Add-EnvironmentPath" {

        BeforeEach {
            $testEnvPath = $Env:PATH

            $basePath = Join-Path $PSScriptRoot "3dbb0853-9792-41ea-b511-51b2dd538e3f"

            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
            $BasicEnvName = "${EnvTestPrefix}Test"

            $testPSep = ([Regex]::Escape([System.IO.Path]::PathSeparator))
        }
        AfterEach {
            $Env:PATH = $testEnvPath
        }

        It "add environment path" {
            $Env:PATH = "";

            $path1 = (Join-Path $basePath "dir_1")
            Add-EnvironmentPath -Path $path1
            $Env:PATH  | Should -BeExactly (($path1) -join $testPSep)

            $path2 = (Join-Path $basePath "dir_2")
            Add-EnvironmentPath -Path $path2
            $Env:PATH  | Should -BeExactly (($path1, $path2) -join $testPSep)

        }

        It "add environment path target befor" {

            $paths = @(
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
            ) -join $testPSep

            $Env:PATH = $paths

            $path1 = (Join-Path $basePath "dir_1")
            Add-EnvironmentPath -Path $path1 -Befor "*dir2-2-4*","*dir2-1-3*"
            $Env:PATH  | Should -BeExactly (@(
                $path1
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
            ) -join $testPSep)


            $Env:PATH = $paths

            $path1 = (Join-Path $basePath "dir_1")
            Add-EnvironmentPath -Path $path1 -Befor "*dir2-1-3*","*dir2-2-4*"
            $Env:PATH  | Should -BeExactly (@(
                $path1
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
            ) -join $testPSep)

        }

        It "add environment path target after" {

            $paths = @(
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
            ) -join $testPSep

            $Env:PATH = $paths

            $path1 = (Join-Path $basePath "dir_1")
            Add-EnvironmentPath -Path $path1 -After "*dir1-2-1*","*dir1-2-2*"
            $Env:PATH  | Should -BeExactly (@(
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                $path1
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
            ) -join $testPSep)


            $Env:PATH = $paths

            $path1 = (Join-Path $basePath "dir_1")
            Add-EnvironmentPath -Path $path1 -After "*dir1-2-2*","*dir1-2-1*"
            $Env:PATH  | Should -BeExactly (@(
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                $path1
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
            ) -join $testPSep)

        }

        It "add environment path target befor after" {

            $paths = @(
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
            ) -join $testPSep

            $Env:PATH = $paths

            $path1 = (Join-Path $basePath "dir_1")
            Add-EnvironmentPath -Path $path1 -After "*dir1-2-2*" -Befor "*dir2-2-4*"
            $Env:PATH  | Should -BeExactly (@(
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                $path1
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
            ) -join $testPSep)


            $Env:PATH = $paths

            $path1 = (Join-Path $basePath "dir_1")
            $warning = Add-EnvironmentPath -Path $path1 -After "*dir2-2-4*" -Befor "*dir1-2-2*" 3>&1
            $warning | Should -BeExactly ("Conflict add position Befor(1) < After(4)")
            $Env:PATH  | Should -BeExactly (@(
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
            ) -join $testPSep)

        }


        It "add environment path by EnvironmentVariableName" {

            $Env:PATH = $basePath;

            $paths = @(
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
            ) -join $testPSep

            Set-Content -LiteralPath "Env:\${BasicEnvName}" -Value $paths

            $path1 = (Join-Path $basePath "dir_1")

            $path1 | Add-EnvironmentPath -EnvironmentVariableName $BasicEnvName

            Get-Content -LiteralPath "Env:\${BasicEnvName}"  | Should -BeExactly (@(
                (Join-Path $basePath "dir1-1-1")
                (Join-Path $basePath "dir1-2-2")
                (Join-Path $basePath "dir2-1-3")
                (Join-Path $basePath "dir2-2-4")
                $path1
            ) -join $testPSep)

            $Env:PATH  | Should -BeExactly $basePath

        }

    }
}
