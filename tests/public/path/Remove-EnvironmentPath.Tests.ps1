$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Remove-EnvironmentPath" {

        BeforeEach {
            $testEnvPath = $Env:PATH

            $basePath = Join-Path $PSScriptRoot "3dbb0853-9792-41ea-b511-51b2dd538e3f"

            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
            $BasicEnvName = "${EnvTestPrefix}Test"

            $testPSep = ([Regex]::Escape([System.IO.Path]::PathSeparator))
        }
        AfterEach {
            $Env:PATH = $testEnvPath
        }

        It "remove environment path" {

            $Env:PATH = $basePath;

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            $exactlyPath = ($path_exixts, $Env:PATH) -join $testPSep

            $Env:PATH = ($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep


            Remove-EnvironmentPath -Path $path_notexixts
            $Env:PATH  | Should -BeExactly $exactlyPath


        }

        It "remove environment path removal trailing separator" {

            $Env:PATH = $basePath + "/";

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            $exactlyPath = ($path_exixts, $basePath) -join $testPSep

            $Env:PATH = ($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep

            Remove-EnvironmentPath -Path ($path_notexixts + "\")
            $Env:PATH  | Should -BeExactly $exactlyPath

        }

        It "remove environment path like" {

            $Env:PATH = $basePath;

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            $Env:PATH = ($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep


            Remove-EnvironmentPath -Path (Join-Path $basePath "*exixts")
            $Env:PATH  | Should -BeExactly $basePath


        }

        It "remove environment literal path" {

            $path_notexixts1 = (Join-Path $basePath "notexixts1")
            $path_notexixts2 = (Join-Path $basePath "notexixts2")
            $path_notexixts3 = (Join-Path $basePath "notexixts?")


            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2, $path_notexixts3) -join $testPSep
            Remove-EnvironmentPath -LiteralPath (Join-Path $basePath "notexixts1")
            $Env:PATH  | Should -BeExactly (($basePath, $path_notexixts2, $path_notexixts3) -join $testPSep)



            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2, $path_notexixts3) -join $testPSep
            Remove-EnvironmentPath -LiteralPath (Join-Path $basePath "notexixts*")
            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2, $path_notexixts3) -join $testPSep



            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2, $path_notexixts3) -join $testPSep
            Remove-EnvironmentPath -LiteralPath (Join-Path $basePath "notexixts?")
            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2) -join $testPSep


        }

        It "remove environment literal path case sensitive" {

            if (-not $script:IsCaseSensitive) {
                return
            }

            $path_notexixts1 = (Join-Path $basePath "notexixts1")
            $path_notexixts2 = (Join-Path $basePath "notexixts2")

            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2) -join $testPSep


            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2) -join $testPSep
            Remove-EnvironmentPath -LiteralPath (Join-Path $basePath "notexixts1")
            $Env:PATH  | Should -BeExactly (($basePath, $path_notexixts2) -join $testPSep)



            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2) -join $testPSep
            Remove-EnvironmentPath -LiteralPath (Join-Path $basePath "Notexixts1")
            $Env:PATH  | Should -BeExactly (($path_notexixts1, $basePath, $path_notexixts2) -join $testPSep)


        }

        It "remove environment literal path case insensitive" {

            if ($script:IsCaseSensitive) {
                return
            }

            $path_notexixts1 = (Join-Path $basePath "notexixts1")
            $path_notexixts2 = (Join-Path $basePath "notexixts2")

            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2) -join $testPSep


            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2) -join $testPSep
            Remove-EnvironmentPath -LiteralPath (Join-Path $basePath "notexixts1")
            $Env:PATH  | Should -BeExactly (($basePath, $path_notexixts2) -join $testPSep)



            $Env:PATH = ($path_notexixts1, $basePath, $path_notexixts2) -join $testPSep
            Remove-EnvironmentPath -LiteralPath (Join-Path $basePath "Notexixts1")
            $Env:PATH  | Should -BeExactly (($basePath, $path_notexixts2) -join $testPSep)


        }

        It "remove environment remove all duplicate path" {

            $path1 = (Join-Path $basePath "notexixts1")
            $path2 = (Join-Path $basePath "notexixts2")
            $path3 = (Join-Path $basePath "notexixts?")


            $p = ($path1, $basePath, $path2, $path3, $path1, $basePath, $path2, $path3, $path1, $basePath, $path2, $path3, $path1, $basePath, $path2, $path3) -join $testPSep
            $Env:PATH = $p
            $Env:PATH  | Should -BeExactly $p
            Remove-EnvironmentPath -LiteralPath (Join-Path $basePath "notexixts?")
            $Env:PATH  | Should -BeExactly (($path1, $basePath, $path2) -join $testPSep)



            $p = ($path1, $basePath, $path2, $path3, $path1, $basePath, $path2, $path3, $path1, $basePath, $path2, $path3, $path1, $basePath, $path2, $path3) -join $testPSep
            $Env:PATH = $p
            $Env:PATH  | Should -BeExactly $p
            Remove-EnvironmentPath -LiteralPath (Join-Path $basePath "notexixts")
            $Env:PATH  | Should -BeExactly (($path1, $basePath, $path2, $path3) -join $testPSep)

        }

        It "remove environment path by EnvironmentVariableName" {

            $Env:PATH = $basePath;

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            $exactlyPath = ($path_exixts, $Env:PATH) -join $testPSep

            $env = ($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep

            $Env:PATH = $env
            Set-Content -LiteralPath "Env:\${BasicEnvName}" -Value (($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep)


            Remove-EnvironmentPath -Path $path_notexixts -EnvironmentVariableName $BasicEnvName
            Get-Content -LiteralPath "Env:\${BasicEnvName}"  | Should -BeExactly $exactlyPath

            $Env:PATH  | Should -BeExactly $env



        }

        It "remove empty environment path " {

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            Set-Content -LiteralPath "Env:\${BasicEnvName}" -Value (($path_exixts, $path_notexixts) -join $testPSep)

            Remove-EnvironmentPath -Path $path_exixts -EnvironmentVariableName $BasicEnvName
            Get-Content -LiteralPath "Env:\${BasicEnvName}"  | Should -BeExactly $path_notexixts

            Remove-EnvironmentPath -Path $path_exixts -EnvironmentVariableName $BasicEnvName
            Get-Content -LiteralPath "Env:\${BasicEnvName}"  | Should -BeExactly $path_notexixts

            Remove-EnvironmentPath -Path $path_notexixts -EnvironmentVariableName $BasicEnvName
            Test-Path -LiteralPath "Env:\${BasicEnvName}" | Should -BeFalse

            Remove-EnvironmentPath -Path $path_exixts -EnvironmentVariableName $BasicEnvName
            Test-Path -LiteralPath "Env:\${BasicEnvName}" | Should -BeFalse

        }

    }
}
