$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {

    Describe "Test-EnvironmentPath" {

        BeforeEach {
            $testEnvPath = $Env:PATH
            $basePath = Join-Path $PSScriptRoot "3dbb0853-9792-41ea-b511-51b2dd538e3f"

            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
            $BasicEnvName = "${EnvTestPrefix}Test"

            $testPSep = ([Regex]::Escape([System.IO.Path]::PathSeparator))

        }

        AfterEach {
            $Env:PATH = $testEnvPath
        }

        It "test environment path" {

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            $Env:PATH = ($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep


            Test-EnvironmentPath -Path $path_exixts  | Should -BeExactly $true




            Test-EnvironmentPath -Path $path_notexixts  | Should -BeExactly $true




            Test-EnvironmentPath -Path (Join-Path $basePath "unknown")  | Should -BeExactly $false


        }


        It "test environment path like" {

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            $Env:PATH = ($path_notexixts, $Env:PATH, $path_exixts) -join $testPSep



            $pathinfo = ([PSCustomObject]@{
                    Path = (Join-Path $basePath "*xts")
                }), ([PSCustomObject]@{
                    Path = (Join-Path $basePath "*not*")
                }), ([PSCustomObject]@{
                    Path = (Join-Path $basePath "*ABC*")
                })  | Test-EnvironmentPath

            $pathinfo.Count | Should -BeExactly 3

            $pathinfo[0] | Should -BeExactly $true
            $pathinfo[1] | Should -BeExactly $true
            $pathinfo[2] | Should -BeExactly $false



        }

        It "test environment literal path" {

            $path_notexixts1 = (Join-Path $basePath "notexixts1")
            $path_notexixts2 = (Join-Path $basePath "notexixts2")
            $path_notexixts3 = (Join-Path $basePath "notexixts?")

            $Env:PATH = ($path_notexixts1, $Env:PATH, $path_notexixts2, $path_notexixts3) -join $testPSep


            Test-EnvironmentPath -Path (Join-Path $basePath "notexixts*")  | Should -BeExactly $true



            Test-EnvironmentPath -LiteralPath (Join-Path $basePath "notexixts*")  | Should -BeExactly $false



            Test-EnvironmentPath -LiteralPath (Join-Path $basePath "notexixts?")  | Should -BeExactly $true


        }

        It "test environment literal path case sensitive" {

            if (-not $script:IsCaseSensitive) {
                return
            }

            $path_notexixts1 = (Join-Path $basePath "notexixts1")
            $path_notexixts2 = (Join-Path $basePath "notexixts2")

            $Env:PATH = ($path_notexixts1, $Env:PATH, $path_notexixts2) -join $testPSep


            Test-EnvironmentPath -Path (Join-Path $basePath "notexixts1")  | Should -BeExactly $true



            Test-EnvironmentPath -LiteralPath (Join-Path $basePath "Notexixts1")  | Should -BeExactly $false


        }

        It "test environment literal path case insensitive" {

            if ($script:IsCaseSensitive) {
                return
            }

            $path_notexixts1 = (Join-Path $basePath "notexixts1")
            $path_notexixts2 = (Join-Path $basePath "notexixts2")

            $Env:PATH = ($path_notexixts1, $Env:PATH, $path_notexixts2) -join $testPSep


            Test-EnvironmentPath -Path (Join-Path $basePath "notexixts1")  | Should -BeExactly $true



            Test-EnvironmentPath -LiteralPath (Join-Path $basePath "Notexixts1")  | Should -BeExactly $true


        }

        It "test environment path empty" {

            $Env:PATH = ""



            Test-EnvironmentPath -Path "ABC", "XYZ" | Should -BeExactly ($false, $false)



        }

        It "test environment path by EnvironmentVariableName" {

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            Set-Content -LiteralPath "Env:\${BasicEnvName}" -Value (($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep)


            Test-EnvironmentPath -Path $path_exixts | Should -BeExactly $false
            Test-EnvironmentPath -Path $path_notexixts | Should -BeExactly $false



            Test-EnvironmentPath -Path $path_exixts -EnvironmentVariableName $BasicEnvName | Should -BeExactly $true
            Test-EnvironmentPath -Path $path_notexixts -EnvironmentVariableName $BasicEnvName | Should -BeExactly $true


        }

    }
}
