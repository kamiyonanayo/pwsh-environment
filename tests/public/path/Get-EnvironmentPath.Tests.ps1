$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Get-EnvironmentPath" {

        BeforeEach {
            $testEnvPath = $Env:PATH

            $basePath = Join-Path $PSScriptRoot "3dbb0853-9792-41ea-b511-51b2dd538e3f"

            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
            $BasicEnvName = "${EnvTestPrefix}Test"

            $testPSep = ([Regex]::Escape([System.IO.Path]::PathSeparator))
        }
        AfterEach {
            $Env:PATH = $testEnvPath
        }

        It "get environment path" {

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            $Env:PATH = ($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep


            $pathinfo = Get-EnvironmentPath -Path $path_exixts

            $pathinfo.Path | Should -BeExactly $path_exixts
            $pathinfo.Exists | Should -BeExactly $true
            $pathinfo.Path | Should -Exist




            $pathinfo = Get-EnvironmentPath -Path $path_notexixts

            $pathinfo.Path | Should -BeExactly $path_notexixts
            $pathinfo.Exists | Should -BeExactly $false
            $pathinfo.Path | Should -Not -Exist



        }

        It "get environment path like" {

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            $Env:PATH = ($path_notexixts, $Env:PATH, $path_exixts) -join $testPSep

            $pathinfo = ([PSCustomObject]@{
                    Path = (Join-Path $basePath "*exixts")
                })  | Get-EnvironmentPath

            $pathinfo.Count | Should -BeExactly 2

            $pathinfo[0].Path | Should -BeExactly $path_notexixts
            $pathinfo[0].Exists | Should -BeExactly $false
            $pathinfo[0].Path | Should -Not -Exist

            $pathinfo[1].Path | Should -BeExactly $path_exixts
            $pathinfo[1].Exists | Should -BeExactly $true
            $pathinfo[1].Path | Should -Exist

        }

        It "get environment literal path" {

            $path_exixts1 = (Join-Path $basePath "exixts")
            $path_exixts2 = (Join-Path $basePath "exixt*")

            $Env:PATH = ($path_exixts1, $basePath, $path_exixts2) -join $testPSep

            $info = @(Get-EnvironmentPath -LiteralPath (Join-Path $basePath "exixts"))
            $info.Count | Should -BeExactly 1
            $info[0].Path | Should -BeExactly $path_exixts1
            $info[0].Exists | Should -BeExactly $true


            $info = @(Get-EnvironmentPath -LiteralPath (Join-Path $basePath "exixt*"))
            $info.Count | Should -BeExactly 1
            $info[0].Path | Should -BeExactly $path_exixts2
            $info[0].Exists | Should -BeExactly $false

        }

        It "get environment literal path case sensitive" {

            if (-not $script:IsCaseSensitive) {
                return
            }

            $path_exixts1 = (Join-Path $basePath "exixts")
            $path_exixts2 = (Join-Path $basePath "exixt*")

            $Env:PATH = ($path_exixts1, $basePath, $path_exixts2) -join $testPSep

            $info = @(Get-EnvironmentPath -LiteralPath (Join-Path $basePath "exixts"))
            $info.Count | Should -BeExactly 1
            $info[0].Path | Should -BeExactly $path_exixts1
            $info[0].Exists | Should -BeExactly $true


            $info = @(Get-EnvironmentPath -LiteralPath (Join-Path $basePath "Exixts"))
            $info.Count | Should -BeExactly 0

        }

        It "get environment literal path case insensitive" {

            if ($script:IsCaseSensitive) {
                return
            }

            $path_exixts1 = (Join-Path $basePath "exixts")
            $path_exixts2 = (Join-Path $basePath "exixt*")

            $Env:PATH = ($path_exixts1, $basePath, $path_exixts2) -join $testPSep


            $info = @(Get-EnvironmentPath -LiteralPath (Join-Path $basePath "exixts"))
            $info.Count | Should -BeExactly 1
            $info[0].Path | Should -BeExactly $path_exixts1
            $info[0].Exists | Should -BeExactly $true



            $info = @(Get-EnvironmentPath -LiteralPath (Join-Path $basePath "Exixts"))
            $info.Count | Should -BeExactly 1
            $info[0].Path | Should -BeExactly (Join-Path $basePath "exixts")
            $info[0].Exists | Should -BeExactly $true


        }


        It "get environment path empty" {

            $Env:PATH = ""

            $pathinfo = Get-EnvironmentPath

            $pathinfo | Should -BeExactly $null

        }

        It "get environment normalize path pattern 1" {

            $path_exixts = ($basePath + "\" + "exixts")
            $path_notexixts = ($basePath + "/" + "notexixts")

            $Env:PATH = ($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep


            $pathinfo = Get-EnvironmentPath -Path ($basePath + "\" + "exixts")
            $pathinfo.Path | Should -BeExactly $path_exixts
            $pathinfo.Exists | Should -BeExactly $true
            $pathinfo.Path | Should -Exist


            $pathinfo = Get-EnvironmentPath -Path ($basePath + "\" + "notexixts")
            $pathinfo.Path | Should -BeExactly $path_notexixts
            $pathinfo.Exists | Should -BeExactly $false
            $pathinfo.Path | Should -Not -Exist


            $pathinfo = Get-EnvironmentPath -Path ($path_exixts + "\")
            $pathinfo.Path | Should -BeExactly $path_exixts
            $pathinfo.Exists | Should -BeExactly $true
            $pathinfo.Path | Should -Exist



            $pathinfo = Get-EnvironmentPath -Path ($path_exixts + "/")
            $pathinfo.Path | Should -BeExactly $path_exixts
            $pathinfo.Exists | Should -BeExactly $true
            $pathinfo.Path | Should -Exist



            $pathinfo = Get-EnvironmentPath -Path ($path_notexixts + "\")
            $pathinfo.Path | Should -BeExactly $path_notexixts
            $pathinfo.Exists | Should -BeExactly $false
            $pathinfo.Path | Should -Not -Exist



            $pathinfo = Get-EnvironmentPath -Path ($path_notexixts + "/")
            $pathinfo.Path | Should -BeExactly $path_notexixts
            $pathinfo.Exists | Should -BeExactly $false
            $pathinfo.Path | Should -Not -Exist


        }

        It "get environment normalize path pattern 2" {

            $path_exixts = (Join-Path $basePath "exixts") + "\"
            $path_notexixts = (Join-Path $basePath "notexixts") + "/"

            $Env:PATH = ($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep

            $pathinfo = Get-EnvironmentPath -Path ($basePath + "\" + "exixts")
            $pathinfo.Path | Should -BeExactly $path_exixts
            $pathinfo.Exists | Should -BeExactly $true
            $pathinfo.Path | Should -Exist

            $pathinfo = Get-EnvironmentPath -Path ($basePath + "\" + "notexixts")
            $pathinfo.Path | Should -BeExactly $path_notexixts
            $pathinfo.Exists | Should -BeExactly $false
            $pathinfo.Path | Should -Not -Exist



            $pathinfo = Get-EnvironmentPath -Path ($path_exixts + "\")
            $pathinfo.Path | Should -BeExactly $path_exixts
            $pathinfo.Exists | Should -BeExactly $true
            $pathinfo.Path | Should -Exist


            $pathinfo = Get-EnvironmentPath -Path ($path_exixts + "/")
            $pathinfo.Path | Should -BeExactly $path_exixts
            $pathinfo.Exists | Should -BeExactly $true
            $pathinfo.Path | Should -Exist


            $pathinfo = Get-EnvironmentPath -Path ($path_notexixts + "\")
            $pathinfo.Path | Should -BeExactly $path_notexixts
            $pathinfo.Exists | Should -BeExactly $false
            $pathinfo.Path | Should -Not -Exist

            $pathinfo = Get-EnvironmentPath -Path ($path_notexixts + "/")
            $pathinfo.Path | Should -BeExactly $path_notexixts
            $pathinfo.Exists | Should -BeExactly $false
            $pathinfo.Path | Should -Not -Exist


        }


        It "get environment path value only" {

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            $Env:PATH = ($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep


            $pathinfo = Get-EnvironmentPath -Path $path_exixts -ValueOnly

            $pathinfo | Should -BeExactly $path_exixts
            $pathinfo | Should -Exist




            $pathinfo = Get-EnvironmentPath -Path $path_notexixts -ValueOnly

            $pathinfo | Should -BeExactly $path_notexixts
            $pathinfo | Should -Not -Exist



        }

        It "get environment path by EnvironmentVariableName" {

            $path_exixts = (Join-Path $basePath "exixts")
            $path_notexixts = (Join-Path $basePath "notexixts")

            Set-Content -LiteralPath "Env:\${BasicEnvName}" -Value (($path_exixts, $Env:PATH, $path_notexixts) -join $testPSep)



            $pathinfo = Get-EnvironmentPath -Path $path_exixts
            $pathinfo | Should -BeExactly $null




            $pathinfo = Get-EnvironmentPath -Path $path_exixts -EnvironmentVariableName $BasicEnvName

            $pathinfo.Path | Should -BeExactly $path_exixts
            $pathinfo.Exists | Should -BeExactly $true
            $pathinfo.Path | Should -Exist




            $pathinfo = Get-EnvironmentPath -Path $path_notexixts -EnvironmentVariableName $BasicEnvName

            $pathinfo.Path | Should -BeExactly $path_notexixts
            $pathinfo.Exists | Should -BeExactly $false
            $pathinfo.Path | Should -Not -Exist



        }

    }
}
