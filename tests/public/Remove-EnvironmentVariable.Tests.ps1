$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Remove-EnvironmentVariable" {

        BeforeEach {
            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
            $BasicEnvName = "${EnvTestPrefix}Test"
        }
        AfterEach {
            Remove-Item "Env:\${EnvTestPrefix}*"
        }

        It "remove environment variable name" {

            Set-Content -LiteralPath "Env:\${BasicEnvName}" -Value "variable!!"

            Remove-EnvironmentVariable -Name $BasicEnvName

            Test-Path -LiteralPath "Env:\${BasicEnvName}" | Should -BeExactly $false

        }

        It "remove environment variable name like" {

            Set-Content -LiteralPath "Env:\${BasicEnvName}_1_1_1" -Value "1_1_1"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_1_1_2" -Value "1_1_2"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_1_2_1" -Value "1_2_1"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_1_2_2" -Value "1_2_2"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_2_1_1" -Value "2_1_1"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_2_1_2" -Value "2_1_2"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_2_2_1" -Value "2_2_1"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_2_2_2" -Value "2_2_2"

            Remove-EnvironmentVariable -Name "${BasicEnvName}_2_*_2"

            Test-Path -LiteralPath "Env:\${BasicEnvName}_2_1_2" | Should -BeExactly $false
            Test-Path -LiteralPath "Env:\${BasicEnvName}_2_2_2" | Should -BeExactly $false

            (Get-ChildItem "Env:\${BasicEnvName}*").Count | Should -Be 6

        }

        It "remove environment variable literal name" {

            Set-Content -LiteralPath "Env:\${BasicEnvName}_1*" -Value "1*"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_11" -Value "11"

            Remove-EnvironmentVariable -LiteralName "${BasicEnvName}_1*"

            Test-Path -LiteralPath "Env:\${BasicEnvName}_1*" | Should -BeExactly $false
            Test-Path -LiteralPath "Env:\${BasicEnvName}_11" | Should -BeExactly $true

        }

    }
}
