$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Get-EnvironmentVariable" {

        BeforeEach {
            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
            $BasicEnvName = "${EnvTestPrefix}Test"
        }
        AfterEach {
            Remove-Item "Env:\${EnvTestPrefix}*"
        }

        It "get environment variable name" {

            Get-EnvironmentVariable $BasicEnvName | Should -BeExactly $null

            Set-Content -LiteralPath "Env:\${BasicEnvName}" -Value "variable!!"
            $ret = Get-EnvironmentVariable -Name $BasicEnvName

            $ret.Name | Should -BeExactly "${BasicEnvName}"
            $ret.PSPath | Should -BeExactly "Microsoft.PowerShell.Core\Environment::${BasicEnvName}"
            $ret.Value | Should -BeExactly "variable!!"

        }

        It "get environment variable name like" {

            Set-Content -LiteralPath "Env:\${BasicEnvName}_1_1_1" -Value "1_1_1"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_1_1_2" -Value "1_1_2"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_1_2_1" -Value "1_2_1"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_1_2_2" -Value "1_2_2"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_2_1_1" -Value "2_1_1"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_2_1_2" -Value "2_1_2"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_2_2_1" -Value "2_2_1"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_2_2_2" -Value "2_2_2"

            $ret = Get-EnvironmentVariable -Name "${BasicEnvName}_2_*_2"

            $ret.Count | Should -Be 2

            $ret[0].Name | Should -Be "${BasicEnvName}_2_1_2"
            $ret[0].Value | Should -Be "2_1_2"
            $ret[0].PSPath | Should -Be "Microsoft.PowerShell.Core\Environment::${BasicEnvName}_2_1_2"

            $ret[1].Name | Should -Be "${BasicEnvName}_2_2_2"
            $ret[1].Value | Should -Be "2_2_2"
            $ret[1].PSPath | Should -Be "Microsoft.PowerShell.Core\Environment::${BasicEnvName}_2_2_2"

        }

        It "get environment variable literal name" {

            Set-Content -LiteralPath "Env:\${BasicEnvName}_1*" -Value "1*"
            Set-Content -LiteralPath "Env:\${BasicEnvName}_11" -Value "11"

            $ret = Get-EnvironmentVariable -Name "${BasicEnvName}_1*"
            $ret.Count | Should -Be 2

            $ret = Get-EnvironmentVariable -LiteralName "${BasicEnvName}_1*"

            $ret.Name | Should -Be "${BasicEnvName}_1*"
            $ret.Value | Should -Be "1*"
            $ret.PSPath | Should -Be "Microsoft.PowerShell.Core\Environment::${BasicEnvName}_1*"

        }

        It "get environment variable value only" {

            Set-Content -LiteralPath "Env:\${BasicEnvName}" -Value "variable!!"

            $ret = Get-EnvironmentVariable -Name "${BasicEnvName}" -ValueOnly

            $ret | Should -BeExactly "variable!!"

        }

    }
}
