$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Remove-EnvironmentStack" {

        BeforeEach {
            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
            $BasicEnvName = "${EnvTestPrefix}Test"
        }
        AfterEach {
            Clear-InternalEnvironmentStack
            Remove-Item "Env:\${EnvTestPrefix}*"
        }

        It "remove environment stack name" {

            $script:EnvStack.Count | Should -BeExactly 0

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable1"

            Push-EnvironmentStack

            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack[""].Count | Should -BeExactly 1

            $envs =$script:EnvStack[""].Peek();

            $envs[$BasicEnvName].Name | Should -BeExactly $BasicEnvName
            $envs[$BasicEnvName].Value | Should -BeExactly "variable1"

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable2"
            Push-EnvironmentStack

            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack[""].Count | Should -BeExactly 2

            $envs =$script:EnvStack[""].Peek();

            $envs[$BasicEnvName].Name | Should -BeExactly $BasicEnvName
            $envs[$BasicEnvName].Value | Should -BeExactly "variable2"

            Remove-EnvironmentStack

            $script:EnvStack.Count | Should -BeExactly 0


        }

        It "remove environment stack named name" {

            $script:EnvStack.Count | Should -BeExactly 0

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable1"

            Push-EnvironmentStack -StackName "ABC"

            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack["ABC"].Count | Should -BeExactly 1

            $envs =$script:EnvStack["ABC"].Peek();

            $envs[$BasicEnvName].Name | Should -BeExactly $BasicEnvName
            $envs[$BasicEnvName].Value | Should -BeExactly "variable1"

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable2"
            Push-EnvironmentStack

            $script:EnvStack.Count | Should -BeExactly 2
            $script:EnvStack[""].Count | Should -BeExactly 1

            Remove-EnvironmentStack -StackName "abc"

            $script:EnvStack.Count | Should -BeExactly 1

            $script:EnvStack[""].Count | Should -BeExactly 1

            $envs =$script:EnvStack[""].Peek();

            $envs[$BasicEnvName].Name | Should -BeExactly $BasicEnvName
            $envs[$BasicEnvName].Value | Should -BeExactly "variable2"

        }

        It "remove environment stack from pipeline" {

            $script:EnvStack.Count | Should -BeExactly 0

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable1"

            Push-EnvironmentStack -StackName "ABC"

            $script:EnvStack.Count | Should -BeExactly 1
            $script:EnvStack["ABC"].Count | Should -BeExactly 1

            $envs =$script:EnvStack["ABC"].Peek();

            $envs[$BasicEnvName].Name | Should -BeExactly $BasicEnvName
            $envs[$BasicEnvName].Value | Should -BeExactly "variable1"

            Set-EnvironmentVariable -Name "${BasicEnvName}" -Value "variable2"
            Push-EnvironmentStack

            $script:EnvStack.Count | Should -BeExactly 2
            $script:EnvStack[""].Count | Should -BeExactly 1

            [PSCustomObject]@{StackName="abc"} | Remove-EnvironmentStack

            $script:EnvStack.Count | Should -BeExactly 1

            $script:EnvStack[""].Count | Should -BeExactly 1

            $envs =$script:EnvStack[""].Peek();

            $envs[$BasicEnvName].Name | Should -BeExactly $BasicEnvName
            $envs[$BasicEnvName].Value | Should -BeExactly "variable2"

        }

    }
}
