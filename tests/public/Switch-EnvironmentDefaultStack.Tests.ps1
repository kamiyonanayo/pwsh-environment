$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here/../../tests/common/init.ps1"

InModuleScope "pwsh-environment" {
    Describe "Switch-EnvironmentDefaultStack" {

        BeforeEach {
            $EnvTestPrefix = "z-unit-test-pwsh-environment-3dbb0853-9792-41ea-b511-51b2dd538e3f_"
        }
        AfterEach {
            Remove-Item "Env:\${EnvTestPrefix}*"
            $script:DefaultEnvStackName = ""
        }

        It "set environment default stack name" {

            $script:DefaultEnvStackName | Should -BeExactly ""

            Switch-EnvironmentDefaultStack "MyStack1"

            $script:DefaultEnvStackName | Should -BeExactly "MyStack1"

        }

        It "set environment default stack name from pipeline" {

            $script:DefaultEnvStackName | Should -BeExactly ""

            [PSCustomObject]@{StackName="MyStack2"} | Switch-EnvironmentDefaultStack

            $script:DefaultEnvStackName | Should -BeExactly "MyStack2"

        }

    }
}
