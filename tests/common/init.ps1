function getclosestdir($Path, $DirName) {
    $p = $path
    while($true){
        $p = Split-Path $p -Parent

        if($p -eq ""){
            return
        }

        if((Split-Path $p -Leaf) -eq $DirName){
            $p
            return
        }
    }
}
function joinpaths() {
    $base = ""
    foreach ($p in $args) {
        if($base -eq ""){
            $base = $p
        }else{
            $base = Join-Path $base $p
        }
    }
    $base
}

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ModuleRoot = Resolve-Path (joinpaths (getclosestdir $here "tests") ".." "pwsh-environment")
$ModulePath = (Join-Path $ModuleRoot "pwsh-environment.psm1")

Import-Module $ModulePath -Force



