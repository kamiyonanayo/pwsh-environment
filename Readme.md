
# pwsh-environment

This is a module designed for manipulating environment variables.

In addition to functions for adding, updating, and deleting environment variables, as well as managing path-style variables,
you can utilize the Push-EnvironmentStack function to store the current environment variables on a stack.
Subsequently, you can use the Pop-EnvironmentStack function to restore the environment variables to their previous state.

## Installation

This library is installable via [PowerShell Gallery](https://www.powershellgallery.com/packages/pwsh-environment/):

```powershell
Install-Module -Name pwsh-environment
```

## Usage

* Functions:
  * `Get-EnvironmentVariable [Name]` - Gets the Environment Variable at the specified name.
  * `Set-EnvironmentVariable [Name] [Value]` - Changes the value of an Environment Variable to the value specified in the command.
  * `Remove-EnvironmentVariable [Name]` - Deletes the specified Environment Variable.
  * `Test-EnvironmentVariable [Name]` - Determines whether of a Environment Variable exist.
  * `Get-EnvironmentPath [Name]` - Gets information about the current PATH Environment Variable.
  * `Add-EnvironmentPath [Name] [Value]` - Determines whether all elements of a PATH Environment Variable exist.
  * `Remove-EnvironmentPath [Name]` - Deletes the specified PATH Environment Variable.
  * `Test-EnvironmentPath [Name]` - Determines whether all elements of a PATH Environment Variable exist.
  * `Push-EnvironmentStack` - Adds the current Environment Variables to the top of a Environment stack.
  * `Pop-EnvironmentStack` - Changes the current Environment Variable to the Environment Variable most recently pushed onto the stack.
  * `Remove-EnvironmentStack` - Deletes a Environment stack.
  * `Switch-EnvironmentDefaultStack` - Set a specified Default Environment Stack Name.

* Alias:
  * `getenv` - `Get-EnvironmentVariable`
  * `setenv` - `Set-EnvironmentVariable`
  * `rmenv` - `Remove-EnvironmentVariable`
  * `pushenv` - `Push-EnvironmentStack`
  * `popenv` - `Pop-EnvironmentStack`

