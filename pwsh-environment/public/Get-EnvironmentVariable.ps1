#requires -Version 3
Set-StrictMode -Version Latest

function Get-EnvironmentVariable{
    <#
    .SYNOPSIS
        Gets information about the current Environment Variable.
    .DESCRIPTION
        The Get-EnvironmentVariable cmdlet gets an object that represents the current Environment Variable.
    .EXAMPLE
        PS C:\> Get-EnvironmentVariable GOPATH

        Name   Value
        ----   -----
        GOPATH C:\Users\example\gocode
    .EXAMPLE
        PS /home/vagrant> Get-EnvironmentVariable GO*

        Name         Value
        ----         -----
        GOPATH       /home/vagrant/gocode
        GOROOT       /home/vagrant/go
    .EXAMPLE
        PS /home/vagrant> Get-EnvironmentVariable GO* -ValueOnly
        /home/vagrant/gocode
        /home/vagrant/go
    .EXAMPLE
        PS C:\> Get-EnvironmentVariable -LiteralName "A*B*C"

        Name  Value
        ----  -----
        A*B*C Z*Y*Z
    #>
    [CmdletBinding(DefaultParametersetName="Env")]
    [OutputType("PwshEnvironment.Entry")]
    Param(
        # Specifies a Name of the Environment Variable.
        [Parameter(Position=0, ParameterSetName="Env", ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [string[]]$Name
        ,
        # Specifies a Name of the Environment Variable.
        [Parameter(Position=0, Mandatory, ParameterSetName="LiteralName", ValueFromPipelineByPropertyName)]
        [string[]]$LiteralName
        ,
        # Indicates that this cmdlet gets only the value of the Environment Variable.
        [Parameter(ParameterSetName="Env")]
        [Parameter(ParameterSetName="LiteralName")]
        [switch]$ValueOnly = $false
    )
    Begin{
    }
    Process{

        $isLiteralName = $PsCmdlet.ParameterSetName -eq "LiteralName"

        if($isLiteralName){
            $names = $LiteralName
        }else{
            $names = $Name
        }

        if(($null -eq $names)){
            $names = ,"*"
        }

        $names | ForEach-Object {

            $pattern = $_

            if($isLiteralName){
                $params = @{
                    LiteralPath = (Join-Path -Path "${script:EnvDriveName}:" -ChildPath $pattern)
                }
            }else{
                $params = @{
                    Path    = "${script:EnvDriveName}:*"
                    Include = $pattern
                }
            }

            Get-ChildItem @params | Sort-Object Name | ForEach-Object  {
                if($ValueOnly){
                    $_.Value
                }else{
                    New-InternalEntry $_.Name $_.Value $_.PSPath
                }
            }

            # if(($null -eq $pattern) -or ("" -eq $pattern)){
            #     $pattern = "*"
            # }
            # Get-ChildItem Env: | ForEach-Object {
            #     $psPath   = $_.PSPath
            #     $envName  = $_.Name
            #     $envValue = $_.Value
            #     if($isLiteralName){
            #         if($script:IsCaseSensitive){
            #             $hit = $envName -ceq $pattern
            #         }else{
            #             $hit = $envName -ieq $pattern
            #         }
            #     }else{
            #         if($script:IsCaseSensitive){
            #             $hit = $envName -clike $pattern
            #         }else{
            #             $hit = $envName -ilike $pattern
            #         }
            #     }
            #     if($hit){
            #         if($ValueOnly){
            #             $envValue
            #         }else{
            #             New-InternalEntry $envName $envValue $psPath
            #         }
            #     }
            # }
        }
    }
}
