#requires -Version 3
Set-StrictMode -Version Latest

function Set-EnvironmentVariable{
    <#
    .SYNOPSIS
        Set a specified Environment Variable.
    .DESCRIPTION
        The Set-EnvironmentVariable cmdlet set a specified Environment Variable.
    .EXAMPLE
        PS C:\> Set-EnvironmentVariable GOPATH "$HOME/gocode"
    #>
    [CmdletBinding(DefaultParametersetName="Env", SupportsShouldProcess)]
    Param(
        # Specifies a Name of the Environment Variable.
        [Parameter(Position=0, Mandatory, ParameterSetName="Env", ValueFromPipelineByPropertyName)]
        [String]$Name
        ,
        # Specifies a new value for the Environment Variable.
        [Parameter(Position=1, ParameterSetName="Env", ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [String]$Value = ""
    )
    Process
    {
        $envPath = Join-Path -Path "${script:EnvDriveName}:" -ChildPath $Name
        if($PSCmdlet.ShouldProcess("Name: ${Name} Value: ${Value}", "Set Environment Variable")){
            Set-Content -LiteralPath $envPath -Value $Value -Confirm:$false
        }
    }
}
