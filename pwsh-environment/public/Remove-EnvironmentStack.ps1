#requires -Version 3
Set-StrictMode -Version Latest

function Remove-EnvironmentStack{
    <#
    .SYNOPSIS
        Deletes a Environment stack.
    .DESCRIPTION
        The Remove-EnvironmentStack cmdlet deletes a Environment stack
    .EXAMPLE
        PS C:\> Remove-EnvironmentStack -StackName "MyStack"
    #>
    [CmdletBinding(DefaultParametersetName="Stack", SupportsShouldProcess)]
    Param(
        # Environment Stack Name
        [Parameter(Position=0, ParameterSetName="Stack", ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [string[]]$StackName
    )
    Process
    {
        $StackName | ForEach-Object {
            $name = $_
            if($null -eq $name){
                $name = ""
            }
            if($script:EnvStack.Contains($name)){
                if($PSCmdlet.ShouldProcess("StackName: ${name}", "Remove Environment Stack")){
                    $script:EnvStack.Remove($name)
                }
            }
        }
    }
}
