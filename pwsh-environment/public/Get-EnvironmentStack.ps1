#requires -Version 3
Set-StrictMode -Version Latest

function Get-EnvironmentStack {
    <#
    .SYNOPSIS
        Gets information about a Environment stack.
    .DESCRIPTION
        The Get-EnvironmentStack cmdlet gets an object that represents a Environment stack.
    .EXAMPLE
        PS C:\> Get-EnvironmentStack
    .EXAMPLE
        PS C:\> Get-EnvironmentStack -Top
    #>
    [CmdletBinding(DefaultParametersetName="Stack")]
    [OutputType("PwshEnvironment.Stack", ParameterSetName="Stack")]
    [OutputType("PwshEnvironment.EnvHashTable", ParameterSetName="Top")]
    Param(
        # Specifies a Name of the Environment Stack.
        [Parameter(Position=0, ParameterSetName="Stack", ValueFromPipelineByPropertyName)]
        [Parameter(Position=0, ParameterSetName="Top", ValueFromPipelineByPropertyName)]
        [string[]]$StackName = ""
        ,
        # Get Stack top entry
        [Parameter(ParameterSetName="Top")]
        [switch]$Top
    )
    Process {
        $StackName | ForEach-Object {
            $sn = $_
            if($sn -eq ""){
                $sn = $script:DefaultEnvStackName
            }
            if($script:EnvStack.Contains($sn)){
                if($Top){
                    if(0 -lt $script:EnvStack[$sn].Count){
                        ,($script:EnvStack[$sn].Peek() | ForEach-Object {
                            $_.Clone()
                        })
                    }
                }else{
                    ,($script:EnvStack[$sn].Clone())
                }
            }
        }
    }
}
