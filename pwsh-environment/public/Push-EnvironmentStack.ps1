#requires -Version 3
Set-StrictMode -Version Latest

function Push-EnvironmentStack{
    <#
    .SYNOPSIS
        Adds the current Environment Variables to the top of a Environment stack.
    .DESCRIPTION
        The Push-EnvironmentStack cmdlet adds, or pushes, the current Environment Variables onto a Environment stack.
    .EXAMPLE
        PS C:\> Push-EnvironmentStack
    #>
    [CmdletBinding(DefaultParametersetName="Stack")]
    Param(
        # Specifies the Environment stack to which the current Environment Variables is added.
        # Enter a Environment Variables stack name.
        # If the stack does not exist, Push-EnvironmentStack creates it.
        [Parameter(Position=0, ParameterSetName="Stack", ValueFromPipelineByPropertyName)]
        [String]$StackName = ""
        ,
        # Stack Message
        [Parameter(ParameterSetName="Stack", ValueFromPipelineByPropertyName)]
        [String]$Message = ""
    )
    Process
    {

        if($StackName -eq ""){
            $name = $script:DefaultEnvStackName
        }else{
            $name = $StackName
        }

        if(-not $script:EnvStack.Contains($name)){
            $script:EnvStack[$name] = New-InternalStack $name
        }

        $envs = New-InternalEnvHashTable

        Get-EnvironmentVariable | ForEach-Object {
            $envs[$_.Name] = $_
        }

        $envs.Message = $Message

        $script:EnvStack[$name].Push($envs)

    }
}
