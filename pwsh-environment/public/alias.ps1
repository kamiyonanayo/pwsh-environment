
Set-Alias -Name setenv Set-EnvironmentVariable -Scope Script
Set-Alias -Name getenv Get-EnvironmentVariable -Scope Script
Set-Alias -Name rmenv  Remove-EnvironmentVariable -Scope Script

Set-Alias -Name pushenv Push-EnvironmentStack -Scope Script
Set-Alias -Name popenv  Pop-EnvironmentStack -Scope Script
