#requires -Version 3
Set-StrictMode -Version Latest

function Remove-EnvironmentVariable{
    <#
    .SYNOPSIS
        Deletes a Environment Variable and its value.
    .DESCRIPTION
        The Remove-EnvironmentVariable cmdlet deletes a variable and its value
    .EXAMPLE
        PS C:\> Remove-EnvironmentVariable GOPATH
    .EXAMPLE
        PS /vagrant/pwsh-environment> Remove-EnvironmentVariable GO* -WhatIf
        What if: Performing the operation "Remove Environment Variable" on target "Name: GOROOT Value: /home/vagrant/go".
        What if: Performing the operation "Remove Environment Variable" on target "Name: GOPATH Value: /home/vagrant/gocode".
    #>
    [CmdletBinding(DefaultParametersetName="Env", SupportsShouldProcess)]
    Param(
        # Specifies a Name of the Environment Variable.
        [Parameter(Position=0, Mandatory, ParameterSetName="Env", ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [string[]]$Name
        ,
        # Specifies a Name of the Environment Variable.
        [Parameter(Position=0, Mandatory, ParameterSetName="LiteralName", ValueFromPipelineByPropertyName)]
        [string[]]$LiteralName
    )
    Begin
    {
    }
    Process{

        $isLiteralName = $PsCmdlet.ParameterSetName -eq "LiteralName"

        if($isLiteralName){
            $names = $LiteralName
        }else{
            $names = $Name
        }

        if(($null -eq $names)){
            $names = ,"*"
        }

        $names | ForEach-Object {

            $pattern = $_

            if($isLiteralName){
                $params = @{
                    LiteralPath = (Join-Path -Path "${script:EnvDriveName}:" -ChildPath $pattern)
                }
            }else{
                $params = @{
                    Path    = "${script:EnvDriveName}:*"
                    Include = $pattern
                }
            }

            Get-ChildItem @params | Sort-Object Name | ForEach-Object  {
                if($PSCmdlet.ShouldProcess("Name: $($_.Name) Value: $($_.Value)", "Remove Environment Variable")){
                    Remove-Item -LiteralPath $_.PSPath -Confirm:$false
                }
            }

        }
    }
}
