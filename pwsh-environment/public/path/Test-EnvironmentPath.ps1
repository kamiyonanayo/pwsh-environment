#requires -Version 3
Set-StrictMode -Version Latest

function Test-EnvironmentPath{
    <#
    .SYNOPSIS
        Determines whether all elements of a PATH Environment Variable exist.
    .DESCRIPTION
        The Test-EnvironmentPath cmdlet determines whether all elements of the PATH Environment Variable exist. It returns $True if all elements exist and $False if any are missing.
    .EXAMPLE
        PS C:\> Test-EnvironmentPath "*hAsKeLl*"
        True
    #>
    [CmdletBinding(DefaultParametersetName="Path")]
    [OutputType([bool])]
    Param(
        # Specifies a Path Name of the PATH Environment Variable.
        [Parameter(Position=0, ParameterSetName="Path", ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [string[]]$Path
        ,
        # Specifies a Name of the PATH Environment Variable.
        [Parameter(Position=0, Mandatory, ParameterSetName="LiteralPath", ValueFromPipelineByPropertyName)]
        [string[]]$LiteralPath
        ,
        # Specifies a Name of the Environment Variable.
        [Parameter(ParameterSetName="Path", ValueFromPipelineByPropertyName)]
        [Parameter(ParameterSetName="LiteralPath", ValueFromPipelineByPropertyName)]
        [string]$EnvironmentVariableName = "PATH"
    )
    Begin{
    }
    Process{

        $isLiteralPath = $PsCmdlet.ParameterSetName -eq "LiteralPath"

        $hashEnv = Test-InternalEnvPathArrayNotEmpty $EnvironmentVariableName

        if($isLiteralPath){
            $names = $LiteralPath
        }else{
            if(($null -eq $Path)){
                $names = [string[]]"*"
            }else{
                $names = $Path
            }
        }

        $names | ForEach-Object {

            if($hashEnv){

                $pattern = $_

                if($isLiteralPath){
                    $params = @{
                        LiteralPath = $pattern
                    }
                }else{
                    $params = @{
                        Path    = $pattern
                    }
                }

                (@(Get-EnvironmentPath @params -EnvironmentVariableName $EnvironmentVariableName).Count -ne 0)

            }else{

                $false

            }

        }
    }
}
