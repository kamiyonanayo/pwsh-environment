#requires -Version 3
Set-StrictMode -Version Latest

function Remove-EnvironmentPath{
    <#
    .SYNOPSIS
        Deletes the specified PATH Environment Variable.
    .DESCRIPTION
        The Remove-EnvironmentPath cmdlet deletes one or more PATH Environment Variable.
    .EXAMPLE
        PS C:\> Remove-EnvironmentPath "*/jdk1.8.0_102/*"
    #>
    [CmdletBinding(DefaultParametersetName="Path", SupportsShouldProcess)]
    Param(
        # Specifies a Path Name of the PATH Environment Variable.
        [Parameter(Position=0, Mandatory, ParameterSetName="Path", ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [string[]]$Path
        ,
        # Specifies a Name of the PATH Environment Variable.
        [Parameter(Position=0, Mandatory, ParameterSetName="LiteralPath", ValueFromPipelineByPropertyName)]
        [string[]]$LiteralPath
        ,
        # Specifies a Name of the Environment Variable.
        [Parameter(ParameterSetName="Path", ValueFromPipelineByPropertyName)]
        [Parameter(ParameterSetName="LiteralPath", ValueFromPipelineByPropertyName)]
        [string]$EnvironmentVariableName = "PATH"
    )
    Begin{
    }
    Process{

        $isLiteralPath = $PsCmdlet.ParameterSetName -eq "LiteralPath"

        $paths = @(Get-InternalEnvPathArray $EnvironmentVariableName)

        if($paths.Count -eq 0){
            return
        }

        if($isLiteralPath){
            $names = $LiteralPath
        }else{
            if(($null -eq $Path)){
                $names = [string[]]"*"
            }else{
                $names = $Path
            }
        }

        $names | ForEach-Object {

            $pattern = $_ | Convert-InternalNormalizeDirPath

            if($isLiteralPath){
                if($IsCaseSensitive){
                    $comparator  = {
                        $_.NormalizePath -cne $pattern
                    }
                }else{
                    $comparator  = {
                        $_.NormalizePath -ine $pattern
                    }
                }
            }else{
                if([string]::IsNullOrEmpty($pattern)){
                    $pattern = "*"
                }
                $comparator  = {
                    $_.NormalizePath -inotlike $pattern
                }
            }

            $paths = @($paths | Where-Object $comparator)

        }

        if($PSCmdlet.ShouldProcess("Name: $($EnvironmentVariableName), Path: $($names -join ",")", "Remove Environment Path")){
            Set-InternalEnvPathArray $paths $EnvironmentVariableName -Confirm:$false
        }

    }
}
