#requires -Version 3
Set-StrictMode -Version Latest

function Get-EnvironmentPath{
    <#
    .SYNOPSIS
        Gets information about the current PATH Environment Variable.
    .DESCRIPTION
        The Get-EnvironmentPath cmdlet gets an object that represents the current PATH Environment Variable.
    .EXAMPLE
        PS C:\> Get-EnvironmentPath "*hAsKeLl*"

        Path                                                      Exists
        ----                                                      ------
        C:\Program Files\Haskell\bin                               False
        C:\Program Files\Haskell Platform\8.0.2\lib\extralibs\bin   True
        C:\Program Files\Haskell Platform\8.0.2\bin                 True
        C:\Program Files\Haskell Platform\8.0.2\mingw\bin           True
    #>
    [CmdletBinding(DefaultParametersetName="Path")]
    [OutputType("PwshEnvironment.PathInfo")]
    [OutputType([string])]
    Param(
        # Specifies a Path Name of the PATH Environment Variable.
        [Parameter(Position=0, ParameterSetName="Path", ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [string[]]$Path
        ,
        # Specifies a Name of the PATH Environment Variable.
        [Parameter(Position=0, Mandatory, ParameterSetName="LiteralPath", ValueFromPipelineByPropertyName)]
        [string[]]$LiteralPath
        ,
        # Indicates that this cmdlet gets only the value of the PATH Environment Variable.
        [Parameter(ParameterSetName="Path")]
        [Parameter(ParameterSetName="LiteralPath")]
        [switch]$ValueOnly = $false
        ,
        # Specifies a Name of the Environment Variable.
        [Parameter(ParameterSetName="Path", ValueFromPipelineByPropertyName)]
        [Parameter(ParameterSetName="LiteralPath", ValueFromPipelineByPropertyName)]
        [string]$EnvironmentVariableName = "PATH"
    )
    Begin{
    }
    Process{

        $isLiteralPath = $PsCmdlet.ParameterSetName -eq "LiteralPath"

        $paths = @(Get-InternalEnvPathArray $EnvironmentVariableName)

        if($paths.Count -eq 0){
            return
        }

        if($isLiteralPath){
            $names = $LiteralPath
        }else{
            if(($null -eq $Path)){
                $names = [string[]]"*"
            }else{
                $names = $Path
            }
        }

        $names | ForEach-Object {

            $pattern = $_ | Convert-InternalNormalizeDirPath

            if($isLiteralPath){
                if($IsCaseSensitive){
                    $comparator  = {
                        $_.NormalizePath -ceq $pattern
                    }
                }else{
                    $comparator  = {
                        $_.NormalizePath -ieq $pattern
                    }
                }
            }else{
                if([string]::IsNullOrEmpty($pattern)){
                    $pattern = "*"
                }
                $comparator  = {
                    $_.NormalizePath -ilike $pattern
                }
            }

            $paths | Where-Object $comparator | ForEach-Object {
                if($ValueOnly){
                    $_.Path
                }else{
                    New-InternalPathInfo $_.Path
                }
            }

        }
    }
}
