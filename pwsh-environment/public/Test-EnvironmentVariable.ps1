#requires -Version 3
Set-StrictMode -Version Latest

function Test-EnvironmentVariable{
    <#
    .SYNOPSIS
        Determines whether all elements of a Environment Variable exist.
    .DESCRIPTION
        The Test-EnvironmentVariable cmdlet determines whether all elements of the Environment Variable exist. It returns $True if all elements exist and $False if any are missing.
    .EXAMPLE
        PS C:\> Test-EnvironmentVariable "JAVA_*"
        True
    #>
    [CmdletBinding(DefaultParametersetName="Env")]
    Param(
        # Specifies a Path Name of the Environment Variable.
        [Parameter(Position=0, ParameterSetName="Env", ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [string[]]$Name
        ,
        # Specifies a Name of the Environment Variable.
        [Parameter(Position=0, Mandatory, ParameterSetName="LiteralName", ValueFromPipelineByPropertyName)]
        [string[]]$LiteralName
    )
    Begin{
    }
    Process{

        $isLiteralName = $PsCmdlet.ParameterSetName -eq "LiteralName"

        if($isLiteralName){
            $names = $LiteralName
        }else{
            $names = $Name
        }

        if(($null -eq $names)){
            $names = ,"*"
        }

        @($names) | ForEach-Object {

            $pattern = $_

            if($isLiteralName){
                (@(Get-EnvironmentVariable -LiteralName $pattern -ErrorAction SilentlyContinue).Count -ne 0)
            }else{
                (@(Get-EnvironmentVariable -Name $pattern).Count -ne 0)
            }

        }
    }
}
