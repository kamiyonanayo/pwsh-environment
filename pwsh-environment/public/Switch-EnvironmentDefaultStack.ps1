#requires -Version 3
Set-StrictMode -Version Latest

function Switch-EnvironmentDefaultStack{
    <#
    .SYNOPSIS
        Set a specified Default Environment Stack Name.
    .DESCRIPTION
        The Switch-EnvironmentDefaultStack cmdlet set a specified Default Environment Stack Name.
    .EXAMPLE
        PS C:\> Switch-EnvironmentDefaultStack "MyStack"
    #>
    [CmdletBinding(DefaultParametersetName="Stack", SupportsShouldProcess)]
    Param(
        # Specifies the Environment stack name that this cmdlet makes the current Environment stack.
        # Enter a Environment stack name.
        # To indicate the unnamed default Environment stack, type $Null" or an empty string ("").
        [Parameter(Position=0,ParameterSetName="Stack", ValueFromPipelineByPropertyName)]
        [String]$StackName = ""
    )
    Process
    {
        if($PSCmdlet.ShouldProcess("StackName: ${StackName}", "Set Default Environment Stack Name")){
            $script:DefaultEnvStackName = $StackName
        }
    }
}
