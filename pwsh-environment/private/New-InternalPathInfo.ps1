#requires -Version 3
Set-StrictMode -Version Latest
function New-InternalPathInfo(){
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseShouldProcessForStateChangingFunctions', '', Scope='Function')]
    Param($Path)

    $new = [PSCustomObject]@{
        PSTypeName  = "PwshEnvironment.PathInfo"
        Path        = $Path
    }

    $new | Add-Member -MemberType ScriptProperty -Name "Exists" -Value {
        (Test-Path -LiteralPath $this.Path)
    }

    $new

}
