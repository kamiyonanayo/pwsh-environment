#requires -Version 3
Set-StrictMode -Version Latest
function Get-InternalEnvPathArray($EnvironmentVariableName = "PATH") {
    ,@(Get-EnvironmentVariable -LiteralName $EnvironmentVariableName -ValueOnly -ErrorAction SilentlyContinue) -split $script:PathSeparatorReg | Where-Object { $_ -ne "" } | ForEach-Object {
        New-InternalEnvValue $_
    }
}
