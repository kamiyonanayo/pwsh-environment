#requires -Version 3
Set-StrictMode -Version Latest
function Set-InternalEnvPathArray{
    [CmdletBinding(SupportsShouldProcess)]
    Param(
        [Parameter(Position=0, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        $Path
        ,
        [Parameter(Position=1)]
        $EnvironmentVariableName = "PATH"
    )
    Begin{
        $pathhash = New-InternalHashTbale
    }
    Process{
        @($Path) | Where-Object { $_.Path -ne "" } | ForEach-Object {
            $pathhash[$_.NormalizePath] = $_.Path
        }
    }
    End{
        Set-EnvironmentVariable -Name $EnvironmentVariableName -Value (([string[]]$pathhash.Keys) -join $script:PathSeparator)
    }
}
