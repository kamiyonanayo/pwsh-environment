#requires -Version 3
Set-StrictMode -Version Latest
function New-InternalEnvHashTable{
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseShouldProcessForStateChangingFunctions', '', Scope='Function')]
    Param()

    $h = New-InternalHashTbale
    [void]$h.PSObject.TypeNames.Insert(0, "PwshEnvironment.EnvHashTable")

    $h | Add-Member -Force -MemberType ScriptMethod -Name Clone -Value {

        $c = New-InternalEnvHashTable

        foreach ($key in $this.Keys){
            $c[$key] = $this[$key].Clone()
        }

        $c.Message = $this.Message

        $c

    }

    $h | Add-Member -MemberType NoteProperty -Name "Message" -Value ""

    $h

}
