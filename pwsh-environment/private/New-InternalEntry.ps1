#requires -Version 3
Set-StrictMode -Version Latest
function New-InternalEntry(){
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseShouldProcessForStateChangingFunctions', '', Scope='Function')]
    Param($Name, $Value, $PSPath)

    $new = [PSCustomObject]@{
        PSTypeName  = 'PwshEnvironment.Entry'
        Name        = $Name
        Value       = $Value
        PSPath      = $PSPath
    }

    $new | Add-Member -MemberType ScriptMethod -Name Clone -Value {
        $this.PSObject.Copy()
    }

    $new

}
