#requires -Version 3
Set-StrictMode -Version Latest
function Clear-InternalEnvironmentStack{
    $script:EnvStack = New-Object System.Collections.Specialized.OrderedDictionary ([StringComparer]::OrdinalIgnoreCase)
    $script:DefaultEnvStackName = "";
}
