#requires -Version 3
Set-StrictMode -Version Latest
filter Convert-InternalNormalizeDirPath{
    [string]$p = $_ | Convert-InternalNormalizePath
    if($p -eq $script:DirectorySeparatorChar){
        $p
    }elseif($p.EndsWith($script:DirectorySeparatorChar)){
        $p.Substring(0, $p.Length - $script:DirectorySeparatorChar.Length)
    }else{
        $p
    }
}
