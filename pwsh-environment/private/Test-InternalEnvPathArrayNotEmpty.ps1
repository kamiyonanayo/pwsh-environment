#requires -Version 3
Set-StrictMode -Version Latest
function Test-InternalEnvPathArrayNotEmpty{
    $paths = (Get-EnvironmentVariable -LiteralName $EnvironmentVariableName -ValueOnly -ErrorAction SilentlyContinue) -split $script:PathSeparatorReg
    foreach ($path in $paths) {
        if(-not [string]::IsNullOrEmpty($path)){
            $true
            return
        }
    }
    $false
}
