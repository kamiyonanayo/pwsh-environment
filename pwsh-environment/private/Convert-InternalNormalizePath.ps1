#requires -Version 3
Set-StrictMode -Version Latest
filter Convert-InternalNormalizePath{
    $_ -replace "[\\/]+",$script:DirectorySeparatorChar
}
