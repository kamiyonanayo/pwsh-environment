#requires -Version 3
Set-StrictMode -Version Latest
function New-InternalEnvValue() {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseShouldProcessForStateChangingFunctions', '', Scope='Function')]
    Param($path)

    [PSCustomObject]@{
        Path            = $path
        NormalizePath   = ($path | Convert-InternalNormalizeDirPath)
    }
}
