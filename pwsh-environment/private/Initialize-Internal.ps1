#requires -Version 3
Set-StrictMode -Version Latest
function Initialize-Internal(){
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidAssignmentToAutomaticVariable', '', Scope='Function')]
    Param()

    if(-not (Test-Path Microsoft.PowerShell.Core\Variable::IsCoreCLR)){
        $IsCoreCLR = $false;
    }
    if(-not (Test-Path Microsoft.PowerShell.Core\Variable::IsWindows)){
        $IsWindows = $true;
    }

    Set-Variable -Scope script -option None EnvDriveName                "Env"
    Set-Variable -Scope script -option None IsCaseSensitive             ($IsCoreCLR -and (-not $IsWindows))
    Set-Variable -Scope script -option None DirectorySeparatorChar      ([string][System.IO.Path]::DirectorySeparatorChar)
    Set-Variable -Scope script -option None PathSeparator               ([string][System.IO.Path]::PathSeparator)
    Set-Variable -Scope script -option None DirectorySeparatorCharReg   ([regex]::Escape($DirectorySeparatorChar))
    Set-Variable -Scope script -option None PathSeparatorReg            ([regex]::Escape($PathSeparator))

    if($IsCaseSensitive){
        Set-Variable -Scope script -option None StringComparer      ([StringComparer]::Ordinal)
    }else{
        Set-Variable -Scope script -option None StringComparer      ([StringComparer]::OrdinalIgnoreCase)
    }

    [System.Collections.Specialized.OrderedDictionary]$script:EnvStack = New-Object System.Collections.Specialized.OrderedDictionary ([StringComparer]::OrdinalIgnoreCase)
    [string]$script:DefaultEnvStackName = "";

}
