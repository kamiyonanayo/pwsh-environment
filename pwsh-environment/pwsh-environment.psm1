#requires -Version 3
Set-StrictMode -Version Latest

Set-Variable -Scope script -option None ModuleRoot $PSScriptRoot

(Get-ChildItem -Path "$ModuleRoot/private/*.ps1" -Recurse -ErrorAction SilentlyContinue ) | Sort-Object DirectoryName,Name | ForEach-Object {
    . $_.Fullname
}
(Get-ChildItem -Path "$ModuleRoot/public/*.ps1" -Recurse -ErrorAction SilentlyContinue ) | Sort-Object DirectoryName,Name | ForEach-Object {
    . $_.Fullname
}

Initialize-Internal

Export-ModuleMember -Function "*-Environment*" -Alias "*"
